import socket
import sys
import machine

# http://docs.micropython.org/en/latest/esp8266/tutorial/network_tcp.html
def http_get(url):
    _, _, host, path = url.split('/', 3)
    addr = socket.getaddrinfo(host, 3000)[0][-1]
    s = socket.socket()
    s.connect(addr)
    s.send(bytes('GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n' % (path, host), 'utf8'))
    while True:
        data = s.recv(100)
        if data:
            print(str(data, 'utf8'))
        else:
            break
    s.close()

#
# try:
#     http_get('http://10.0.0.201/')
# except:
#     f = open('log2.txt', 'w')
#     f.write(sys.exc_info()[0])
#     f.close()


# sets up all pins as output


pins = [machine.Pin(i, machine.Pin.OUT) for i in (0, 2, 4, 5, 12, 13, 14, 15)]

html = """<!DOCTYPE html>
<html>
    <head> <title>ESP8266 Pins</title> </head>
    <body> <h1>ESP8266 Pins</h1>
        <table border="1"> <tr><th>Pin</th><th>Value</th></tr> %s </table>
    </body>
</html>
"""


address = socket.getaddrinfo('0.0.0.0', 80)[0][-1]

s = socket.socket()
s.bind(address)
s.listen(1)

print('listening on', address)

while True:
    clientSocket, address = s.accept()
    print('*******************client connected from', address)

    reqData = clientSocket.recv(10)
    if not reqData:
        print('NO DATA!? - SHIIIT')
    req = str(reqData).split('\\r\\n')
    reqPath = req[0].split(' ')[1]


    relayPin = pins[0]
    # pinValue = relayPin.value()
    # print(pinValue)
    # print(relayPin)
    # if pinValue == 0:
    #     print('turning on')
    #     relayPin.value(1)
    # else:
    #     print('turning off')
    #     relayPin.value(0)
    print('requested ' + reqPath)

    if reqPath == '/on':
        relayPin.value(1)
    elif reqPath == '/off':
        relayPin.value(0)

# for some reason this code is not working in a browser, only with curl
# the process seems to be hanging / not responding. seems like
# we're not telling chrome when the response is complete?

    cl_file = clientSocket.makefile('rwb', 0)
    while True:
        line = cl_file.readline()
        if not line or line == b'\r\n':
            break
    rows = ['<tr><td>%s</td><td>%d</td></tr>' % (str(p), p.value()) for p in pins]
    response = html % '\n'.join(rows)
    clientSocket.send(bytes(response, 'utf-8'))
    clientSocket.close()

# end http://docs.micropython.org/en/latest/esp8266/tutorial/network_tcp.html
