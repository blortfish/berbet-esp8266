run this, then plug in usb
this gets watches for new devices and will spit out the device path
```
sudo dmesg [-w]
```

# Flashing and getting started
https://docs.micropython.org/en/latest/esp8266/tutorial/intro.html
If this is a new install, erase the flash
```
sudo /home/linuxbrew/.linuxbrew/bin/esptool.py --port /dev/ttyUSB0 erase_flash
```

 run the esp tool with the latest software from
 http://micropython.org/download#esp8266 
```
sudo /home/linuxbrew/.linuxbrew/bin/esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect 0 /home/horsey/projects/personal/iot/micropython/ESP8266/esp8266-20190529-v1.11.bin
```

### connect to the device and setup webREPL 
`sudo picocom /dev/ttyUSB0 -b115200`

### import the webrepl
`import webrepl_setup`

### ** ONLY IF AUTO LOAD ISN'T ENABLED ** 
import webrepl
webrepl.start()

### connection to the access_point network

Connect to the new wifi network (micropython_{mac addr} or something like it)
the password is `micropythoN`

really cool way to upload startup files using websockets: https://github.com/micropython/webrepl
 
### configuring network
```
import network

station_if = network.WLAN(network.STA_IF)
station_if.active(True)

accessPoint_if = network.WLAN(network.AP_IF)
accessPoint_if.active(True)

# connect to wifi
station_if.connect('BatCave 2.4', '$MXcn!2Y@x^fU41')

# get assigned ip address
station_if.ifconfig()

```


### REPL

pin = machine.Pin(0, machine.Pin.OUT)

pin.value(0)
pin.off()

pin.value(1)
pin.on()